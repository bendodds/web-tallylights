import appstate
import itertools
import os
import pythoncom
import time
import win32com.client


#
# Separate thread for midi interface
#
def Run():
    try:
        pythoncom.CoInitialize()

        class EventHandler:
            def OnSysExInput(self, bStrSysEx):
                mox.SendSysExString(bStrSysEx)

            def OnTerminateMidiInput(self):
                pass

            def OnMidiInput(self, nTimestamp, port, status, data1, data2):
                mox.OutputMidiMsg(-1, status, data1, data2)

        def Transition(direction):
            assert direction in ("up", "down")

            directionIndicator = 0x14 if direction == "up" else 0x15
            return [
                (2, 0xB0, directionIndicator, 0x00),
                (2, 0xB0, directionIndicator, 0x7F),
            ]

        def FadeMode(mode):
            assert mode in ("mix", "wipe", "cut")

            if mode == "wipe":
                modeIndicator = 0x00
            elif mode == "mix":
                modeIndicator = 0x01
            else:
                modeIndicator = 0x02

            return [
                (2, 0xB0, 0x13, modeIndicator),
            ]

        def SelectInput(bank, inputNumber):
            assert bank in (0, 1)
            assert inputNumber in (0, 1, 2, 3)

            return [
                (2, 0xB0, 0x00, bank),
                (2, 0xB0, 0x20, 0x00),
                (2, 0xC0, inputNumber, 0x00),
            ]

        def Collapse(*listOfLists):
            return itertools.chain.from_iterable(listOfLists)

        def SendActions(mox, actions):
            for nPort, nStatus, nData1, nData2 in actions:
                mox.OutputMidiMsg(nPort, nStatus, nData1, nData2)

        # Load the midi-ox profile
        profilePath = os.path.join(os.path.dirname(__file__), "midiox.ini")
        if not os.path.exists(profilePath):
            time.sleep(2)

            print("")
            print("ERROR:")
            print("MIDI-OX Profile not created, midi interface will not work.")
            print("follow these instructions:")
            print(" - Run MIDI-OX manually")
            print(" - Select Options -> Midi Devices")
            print(" - On the bottom left, select the desired midi device for output.")
            print("   This will make it appear in the top right.")
            print(" - Close Midi Devices, Select File -> Save Profile.")
            print(" - Save the profile as midios.ini in the same folder as server.py:")
            print("     HERE: %s" % profilePath)
            print("")

            while True:
                time.sleep(1)
                action, target = appstate.PopFromMidiQueue()
                
                if action == "Stop":
                    return

        mox = win32com.client.DispatchWithEvents("MIDIOX.MoxScript.1", EventHandler)
        mox.DivertMidiInput = 1
        mox.FireMidiInput = 1
        mox.ShutdownAtEnd = 1

        time.sleep(2)

        print("MIDI-OX app version: %s" % mox.GetAppVersion())
        print("found %s midi devices" % mox.SysMidiInCount)
        deviceName = mox.GetFirstSysMidiInDev()
        while deviceName:
            print("  found device: %r" % deviceName)
            deviceName = mox.GetNextSysMidiInDev()

        mox.LoadProfile(profilePath)

        # Initialize the switch to live on center camera on bank 0.
        cameraPositions = appstate.GetCameraPositions()
        SendActions(
            mox,
            Collapse(
                SelectInput(0, cameraPositions["Center"]),
                Transition("up"),
                SelectInput(1, cameraPositions["Left"]),
            ),
        )

        currentBank = 0

        while True:
            time.sleep(0.25)

            if appstate.GetExit():
                mox.ShouldExitScript = 1
                os.system("taskkill /f /im midiox.exe")
                return

            action, target = appstate.PopFromMidiQueue()
            if action is None:
                continue

            elif action == "GoLive":
                targetIndex = cameraPositions[target]
                bankToLive = 0 if currentBank == 1 else 1
                transitionDirection = "up" if currentBank == 1 else "down"

                SendActions(
                    mox,
                    Collapse(
                        SelectInput(bankToLive, targetIndex),
                        Transition(transitionDirection),
                        SelectInput(currentBank, targetIndex),
                    ),
                )

                currentBank = bankToLive

            elif action == "SetReady":
                targetIndex = cameraPositions[target]
                bankToReady = 0 if currentBank == 1 else 1

                SendActions(
                    mox,
                    SelectInput(bankToReady, targetIndex),
                )

            else:
                print("Unknown action: %r" % action)

    finally:
        print("Exiting MidiMonitor thread.")
