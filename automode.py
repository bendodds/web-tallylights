import appstate
import random
import socketio as clientSocketIO
import time
import traceback

from datetime import datetime
from datetime import timedelta

#
# Separate thread for running auto mode.
#

def Run(port):
    try:
        global AUTO_CONNECTED

        serverConnection = clientSocketIO.Client()
        serverConnection.connect("http://localhost:%s" % port)

        lastAutoEnabledState = None
        nextSelectedAction = None  # Live/Ready
        nextTransitionTime = appstate.GetAutoTime()
        nextTime = None
        while True:
            try:
                stateChangesOccured = False

                if appstate.GetExit():
                    serverConnection.emit("exit")
                    return

                if not serverConnection.connected:
                    lastAutoEnabledState = False
                    time.sleep(0.5)
                    continue

                if not appstate.GetAuto():
                    lastAutoEnabledState = False
                    time.sleep(0.5)
                    continue

                unavailablePositions = list(appstate.GetUnavailablePositions())
                autoPositions = list(appstate.GetAutoPositions())
                availablePositions = [
                    p
                    for p in appstate.GetCameraPositions().keys()
                    if p not in unavailablePositions
                    and p in autoPositions
                ]
                if not availablePositions:
                    lastAutoEnabledState = False
                    time.sleep(0.5)
                    continue

                if not lastAutoEnabledState:
                    nextTime = datetime.now() + timedelta(seconds=nextTransitionTime * 0.8)

                lastAutoEnabledState = True
                time.sleep(0.2)

                currentLive = appstate.GetLiveCamera()
                currentReady = None
                if appstate.GetReadyCameras():
                    try:
                        currentReady = appstate.GetReadyCameras()[0]
                    except IndexError:
                        pass

                currentLiveIndex = (
                    availablePositions.index(currentLive)
                    if currentLive in availablePositions
                    else -1
                )
                currentTime = datetime.now()

                nextPositionIndex = (currentLiveIndex + 1) % len(availablePositions)
                try:
                    nextPosition = availablePositions[nextPositionIndex]
                except IndexError:
                    continue

                nextSelectedAction = "Live" if nextPosition == currentReady else "Ready"

                # Don't do anything if nextPosition is to ready the current live
                # position. This can happen if cameras disconnect or go N/A while
                # auto mode is on such that there is only one camera left. Auto mode
                # is still enabled so that the missing camera can be transitioned
                # away from.
                if nextSelectedAction == "Ready" and nextPosition == currentLive:
                    continue

                if nextTime < currentTime:
                    # First, un-ready all positions
                    for position in appstate.GetReadyCameras():
                        appstate.RemoveReadyCamera(position)
                        stateChangesOccured = True

                    if nextSelectedAction == "Live":
                        appstate.SetLiveCamera(nextPosition)
                        appstate.SetTimeOfLastTransition(datetime.now().isoformat())
                        nextTransitionTime = appstate.GetAutoTime() \
                                + (appstate.GetAutoTimeVariance() * ((random.random() * 2) - 1))
                        nextTime = datetime.now() + timedelta(seconds=nextTransitionTime * 0.8)
                        appstate.PushToMidiQueue("GoLive", nextPosition)
                        stateChangesOccured = True

                    elif nextSelectedAction == "Ready":
                        appstate.AddReadyCamera(nextPosition)
                        nextTime = datetime.now() + timedelta(seconds=nextTransitionTime * 0.2)
                        appstate.PushToMidiQueue("SetReady", nextPosition)
                        stateChangesOccured = True

                if stateChangesOccured:
                    serverConnection.emit("stateChanged")

            except Exception as e:
                # TypeError: WriteFile() argument 1 must be int, not None
                # OSError: handle is closed
                if isinstance(e, OSError):
                    print(e.message)
                    continue

                if isinstance(e, TypeError):
                    print(e.message)
                    continue

                print("")
                print("Error in Auto Mode Loop!")
                print(e)
                print(traceback.format_exc())

    finally:
        print("Exiting AutoMode thread.")