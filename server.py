import appstate
import flask
import logging
import os
import signal
import time

from datetime import datetime
from flask import request
from flask_socketio import emit
from flask_socketio import join_room
from flask_socketio import rooms
from flask_socketio import SocketIO


def Run(port):
    try:
        log = logging.getLogger("werkzeug")
        log.setLevel(logging.ERROR)

        print(f"Starting flask server on port {port}")
        app = flask.Flask(__name__, template_folder="pages")

        @app.route("/host/")
        def host():
            return flask.render_template("host.html")

        @app.route("/")
        def client():
            return flask.render_template("client.html")
        
        # Must be after app routes.
        socketio = SocketIO(app, logger=False, engineio_logger=False)

        @socketio.on("disconnect")
        def Disconnect():
            if "clients" in rooms():
                if request.sid in appstate.GetConnectedCameras():
                    position = appstate.GetConnectedCameras()[request.sid]

                    del appstate.GetConnectedCameras()[request.sid]
                    if (
                        position in appstate.GetAutoPositions()
                        and position not in appstate.GetConnectedCameras().values()
                    ):
                        appstate.RemoveAutoPosition(position)

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("join")
        def Join(data):
            assert data["room"] in ("hosts", "clients")

            roomName = data["room"]
            join_room(roomName)

            emit(
                "registration_options",
                {"options": list(appstate.GetCameraPositions().keys())},
                broadcast=False,
            )

            if roomName == "hosts":
                emit(
                    "special_message_options",
                    [{"key": k, "value": v} for k, v in appstate.GetPossibleSpecialMessages().items()],
                    broadcast=False,
                )
            elif roomName == "clients" and "position" in data and data["position"]:
                appstate.GetConnectedCameras()[request.sid] = data["position"]
                if data["position"] not in appstate.GetAutoPositions():
                    appstate.AddAutoPosition(data["position"])

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("register")
        def Register(data):
            previousPosition = appstate.GetConnectedCameras().get(request.sid)
            appstate.SetConnectedCamera(request.sid, data["position"])

            # Turn auto off if camera is changing position, but only if there isn't
            # another camera on that position.
            if previousPosition and previousPosition not in appstate.GetConnectedCameras().values():
                appstate.RemoveAutoPosition(previousPosition)

            if data["position"] not in appstate.GetAutoPositions():
                appstate.AddAutoPosition(data["position"])

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("getRegistrationOptions")
        def GetRegistrationOptions(data):
            emit(
                "registration_options",
                {"options": list(appstate.GetCameraPositions().keys())},
                broadcast=False,
            )

        @socketio.on("selectLive")
        def SelectLive(data):
            liveCamera = data["position"]

            # If a camera is selected for live manually, kill auto.
            if appstate.GetAuto():
                appstate.SetAuto(False)

            appstate.SetTimeOfLastTransition(datetime.now().isoformat())
            appstate.SetLiveCamera(liveCamera)
            appstate.SetNextCamera(None)
            if data["position"] in appstate.GetReadyCameras():
                appstate.RemoveReadyCamera(data["position"])

            if data["position"] in appstate.GetSpecialMessages():
                appstate.RemoveSpecialMessage(data["position"])

            appstate.PushToMidiQueue("GoLive", liveCamera)
            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("setNotReady")
        def SetNotReady(data):
            if data["position"] in appstate.GetReadyCameras():
                appstate.RemoveReadyCamera(data["position"])

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("setReady")
        def SetReady(data):
            position = data["position"]
            if position not in appstate.GetReadyCameras():
                appstate.AddReadyCamera(position)

            appstate.PushToMidiQueue("SetReady", position)
            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("setAutoMode")
        def SetAutoMode(data):
            appstate.SetAuto(data["value"])
            for position in appstate.GetReadyCameras():
                if position in appstate.GetUnavailablePositions():
                    appstate.RemoveReadyCamera(position)

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("setAutoTime")
        def SetAutoTime(data):
            try:
                timeParts = data["value"].replace("s", "").split("+")
                timeInSeconds = int(timeParts[0])
                if len(timeParts) < 2:
                    varianceInSeconds = 0
                else:
                    varianceInSeconds = int(timeParts[1])
            except ValueError:
                return

            appstate.SetAutoTime(timeInSeconds, varianceInSeconds)

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("setAvailability")
        def SetAvailability(data):
            position = data["position"]
            if data["available"]:
                if position in appstate.GetUnavailablePositions():
                    appstate.RemoveUnavailablePosition(position)
            else:
                if position not in appstate.GetUnavailablePositions():
                    appstate.AddUnavailablePosition(position)

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("setSpecialMessage")
        def SetSpecialMessage(data):
            message = data["value"]
            position = data["position"]

            if message:
                if position == "ALL":
                    positions = list(appstate.GetCameraPositions().keys())
                else:
                    positions = [position]

                for position in positions:
                    appstate.AddSpecialMessage(position, message)

            elif position in appstate.GetSpecialMessages():
                appstate.RemoveSpecialMessage(position)

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("stateChanged")
        def StateChanged():
            emit("state", appstate.GetStateForEmit(), broadcast=True)

        @socketio.on("exit")
        def Exit():
            time.sleep(2)
            os.kill(os.getpid(), signal.SIGINT)

        @socketio.on("toggleAutoEnabled")
        def ToggleAutoEnabled(data):
            position = data["position"]
            if position not in appstate.GetAutoPositions():
                appstate.AddAutoPosition(position)
            elif position in appstate.GetAutoPositions():
                appstate.RemoveAutoPosition(position)

            emit("state", appstate.GetStateForEmit(), broadcast=True)

        # Must be after socket bindings.
        socketio.run(app, host="0.0.0.0", port=port)

    finally:
        print("Exiting server thread.")
