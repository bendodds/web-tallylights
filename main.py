r"""Python flask server to interface with tally lights and midi interface

To install:
  - Install python 3.9 (32-bit)
  - Run these installation commands:
    - .\python.exe -m pip install flask
    - .\python.exe -m pip install flask-socketio
    - .\python.exe -m pip install pywin32
    - .\python.exe -m pip install requests
    - .\python.exe -m pip install python-socketio
    - .\python.exe -m pip install python-socketio[client]
  - Install Midi-Ox using the included installer

Run the server with this command:
  - python.exe server.py
"""


import appstate
import automode
import midimonitor
import server
import sys
import threading
import time


if __name__ == "__main__":
    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    else:
        port = 80

    appstate.Initialize()

    midiProcess = threading.Thread(target=midimonitor.Run)
    midiProcess.start()

    serverProcess = threading.Thread(target=server.Run, args=(port, ))
    serverProcess.start()

    time.sleep(3)
    autoModeProcess = threading.Thread(target=automode.Run, args=(port, ))
    autoModeProcess.start()

    time.sleep(5)
    value = input("----------\nPress enter to stop!\n----------\n")
    print("Server exiting!")

    appstate.SetExit()
    
    midiProcess.join()
    serverProcess.join()
    autoModeProcess.join()

    sys.exit(0)
