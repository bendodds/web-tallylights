""" OBS interface for web-tallylights.

This sets the recording light on and off with OBS.
To use this, the python installation associated with
obs must have flask and flask-socketio installed:
    - python.exe -m pip install flask
    - python.exe -m pip install flask-socketio
    - python.exe -m pip install requests
"""


import obspython as obs
import socketio


URL = None


# defines script description
def script_description():
   return "Connects to the web-tallylights system to enable and disable the recording indicator"

# defines user properties
def script_properties():
    properties = obs.obs_properties_create()

    obs.obs_properties_add_text(properties, "host", "Host ip", obs.OBS_TEXT_DEFAULT)
    obs.obs_properties_add_int(properties, "port", "Port", 1, 100000, 1)

    return properties

# called at startup
def script_load(settings):
    obs.obs_frontend_add_event_callback(FrontEndEvent)

def script_defaults(settings):
    obs.obs_data_set_default_string(settings, "host", "localhost")
    obs.obs_data_set_default_int(settings, "port", 50002)

def script_update(settings):
    global URL

    host = obs.obs_data_get_string(settings, "host")
    port = obs.obs_data_get_int(settings, "port")
    if port == 80:
        URL = "http://%s" % host
    else:
        URL = "http://%s:%s" % (host, port)

def FrontEndEvent(eventCode):
    if eventCode == obs.OBS_FRONTEND_EVENT_RECORDING_STARTED:
        SendRecordingUpdate(True)
    
    elif eventCode == obs.OBS_FRONTEND_EVENT_RECORDING_STOPPED:
        SendRecordingUpdate(False)

def SendRecordingUpdate(isRecording):
    global URL

    sio = socketio.Client()
    print("Connecting to URL: %r" % URL)
    sio.connect(URL)
    sio.emit("setRecording", {"value": isRecording})
