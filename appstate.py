import json
import threading


STATE_INIT = {
    "auto": False,
    "autoPositions": [],
    "autoTime": 15,
    "autoTimeVariance": 2,
    "connectedCameras": {},  # {"socketId": "CamearPosition", ...}
    "exit": False,
    "liveCamera": "Center",
    "midiQueue": [],
    "readyCameras": [],
    "specialMessages": {},  # {"CamearPosition": "Message", ...}
    "timeOfLastTransition": None,
    "transitionTime": 1500,  # In milliseconds
    "unavailablePositions": [],
}

CAMERA_POSITIONS = None
SPECIAL_MESSAGES = None


STATE = None
lock = threading.Semaphore()


def __EnsureStateInitialized():
    if not STATE:
        raise Exception("STATE has not been initialized.")
    

def AddAutoPosition(position):
    __EnsureStateInitialized()
    with lock:
        STATE["autoPositions"].append(position)
    

def AddSpecialMessage(position, message):
    __EnsureStateInitialized()
    with lock:
        STATE["specialMessages"][position] = message
    

def AddReadyCamera(position):
    __EnsureStateInitialized()
    with lock:
        STATE["readyCameras"].append(position)
    

def AddUnavailablePosition(position):
    __EnsureStateInitialized()
    with lock:
        STATE["unavailablePositions"].append(position)
    

def GetAuto():
    __EnsureStateInitialized()
    with lock:
        return STATE["auto"]
    

def GetAutoPositions():
    __EnsureStateInitialized()
    with lock:
        return STATE["autoPositions"]
    

def GetAutoTime():
    __EnsureStateInitialized()
    with lock:
        return STATE["autoTime"]
    

def GetAutoTimeVariance():
    __EnsureStateInitialized()
    with lock:
        return STATE["autoTimeVariance"]
    

def GetCameraPositions():
    if not CAMERA_POSITIONS:
        raise Exception("CAMERA_POSITIONS has not been initialized.")

    with lock:
        return CAMERA_POSITIONS
    

def GetConnectedCameras():
    __EnsureStateInitialized()
    with lock:
        return STATE["connectedCameras"]
    

def GetExit():
    __EnsureStateInitialized()
    with lock:
        return STATE["exit"]


def GetLiveCamera():
    __EnsureStateInitialized()
    with lock:
        return STATE["liveCamera"]


def GetMidiQueue():
    __EnsureStateInitialized()
    with lock:
        return STATE["midiQueue"]
    

def GetPossibleSpecialMessages():
    if not SPECIAL_MESSAGES:
        raise Exception("SPECIAL_MESSAGES has not been initialized.")

    with lock:
        return SPECIAL_MESSAGES


def GetReadyCameras():
    __EnsureStateInitialized()
    with lock:
        return STATE["readyCameras"]


def GetSpecialMessages():
    __EnsureStateInitialized()
    with lock:
        return STATE["specialMessages"]
    

def GetStateForEmit():
    __EnsureStateInitialized()
    with lock:
        stateToEmit = {
            "auto": STATE["auto"],
            "autoPositions": list(STATE["autoPositions"]),
            "autoTime": STATE["autoTime"],
            "autoTimeVariance": STATE["autoTimeVariance"],
            "liveCamera": STATE["liveCamera"],
            "midiQueue": STATE["midiQueue"],
            "readyCameras": list(STATE["readyCameras"]),
            "specialMessages": dict(STATE["specialMessages"]),
            "timeOfLastTransition": STATE["timeOfLastTransition"],
            "transitionTime": STATE["transitionTime"],
            "unavailablePositions": list(STATE["unavailablePositions"]),
        }

        connectedPositionCounts = {}
        for position in CAMERA_POSITIONS:
            connectedPositionCounts[position] = 0
        for socketId, position in dict(STATE["connectedCameras"]).items():
            connectedPositionCounts[position] += 1

        stateToEmit["connectedPositionCounts"] = connectedPositionCounts
        return stateToEmit


def GetTimeOfLastTransition():
    __EnsureStateInitialized()
    with lock:
        return STATE["timeOfLastTransition"]


def GetTransitionTime():
    __EnsureStateInitialized()
    with lock:
        return STATE["transitionTime"]


def GetUnavailablePositions():
    __EnsureStateInitialized()
    with lock:
        return STATE["unavailablePositions"]


def Initialize():
    with lock:
        global STATE
        global STATE_INIT

        print("Initializing application state.")
        LoadSettings()

        STATE = dict(STATE_INIT)
        STATE["connectedCameras"] = dict(STATE_INIT["connectedCameras"])
        STATE["readyCameras"] = list(STATE_INIT["readyCameras"])
        STATE["autoPositions"] = list(STATE_INIT["autoPositions"])
        STATE["specialMessages"] = dict(STATE_INIT["specialMessages"])
        STATE["unavailablePositions"] = list(STATE_INIT["unavailablePositions"])


def LoadSettings():
    global CAMERA_POSITIONS
    global SPECIAL_MESSAGES

    if CAMERA_POSITIONS or SPECIAL_MESSAGES:
        raise Exception("Settings have already been loaded.")

    with open("settings.json", "r", encoding="utf-8") as f:
        settings = json.load(f)
        CAMERA_POSITIONS = settings["CameraPositions"]
        SPECIAL_MESSAGES = settings["SpecialMessages"]


def PopFromMidiQueue():
    __EnsureStateInitialized()
    with lock:
        if STATE["midiQueue"]:
            return STATE["midiQueue"].pop(0)
        else:
            return (None, None)


def PushToMidiQueue(action, target):
    __EnsureStateInitialized()
    with lock:
        STATE["midiQueue"].append((action, target))


def RemoveAutoPosition(position):
    __EnsureStateInitialized()
    with lock:
        STATE["autoPositions"].remove(position)


def RemoveReadyCamera(position):
    __EnsureStateInitialized()
    with lock:
        STATE["readyCameras"].remove(position)


def RemoveSpecialMessage(position):
    __EnsureStateInitialized()
    with lock:
        del STATE["specialMessages"][position]


def RemoveUnavailablePosition(position):
    __EnsureStateInitialized()
    with lock:
        STATE["unavailablePositions"].remove(position)


def SetAuto(enabled):
    __EnsureStateInitialized()
    with lock:
        STATE["auto"] = enabled


def SetAutoTime(time, variance):
    __EnsureStateInitialized()
    with lock:
        STATE["autoTime"] = time
        STATE["autoTimeVariance"] = variance


def SetConnectedCamera(sid, position):
    __EnsureStateInitialized()
    with lock:
        STATE["connectedCameras"][sid] = position


def SetExit():
    __EnsureStateInitialized()
    with lock:
        STATE["exit"] = True


def SetLiveCamera(position):
    __EnsureStateInitialized()
    with lock:
        STATE["liveCamera"] = position


def SetNextCamera(position):
    __EnsureStateInitialized()
    with lock:
        STATE["nextCamera"] = position


def SetTimeOfLastTransition(time):
    __EnsureStateInitialized()
    with lock:
        STATE["timeOfLastTransition"] = time
